
import { createStandardAction, createAsyncAction, AppThunk } from 'typesafe-actions';
import { <%= h.changeCase.pascal(name) %>Items, <%= h.changeCase.pascal(name) %>Errors } from '<%= h.changeCase.pascal(name) %>-Types';

// XXX: Replace with your own standard action implementation
export const setItem = createStandardAction('SET_ITEM').map((item: string): {
  payload: <%= h.changeCase.pascal(name) %>Items;
} => ({
  payload: {
    id: 0,
    item,
  },
}));

// XXX: Kindly update payloads to your own async implementation
export const fetch<%= h.changeCase.pascal(name) %>Async = createAsyncAction(
  'FETCH_<%= h.changeCase.snake(name).toUpperCase() %>_REQUEST',
  'FETCH_<%= h.changeCase.snake(name).toUpperCase() %>_SUCCESS',
  'FETCH_<%= h.changeCase.snake(name).toUpperCase() %>_FAILURE',
)<undefined, <%= h.changeCase.pascal(name) %>Items[], <%= h.changeCase.pascal(name) %>Errors[]>();

export const fetch<%= h.changeCase.pascal(name) %> = (): AppThunk => async (dispatch, getState, { apiClient }) => {
  dispatch(fetch<%= h.changeCase.pascal(name) %>Async.request());
  // dispatch(fetch<%= h.changeCase.pascal(name) %>Async.success());
};

