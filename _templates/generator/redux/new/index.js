module.exports = {
    prompt: ({ args }) => {
      const rootComponentDir = args.destination || 'src/store';
      return {
        rootComponentDir,
      };
    },
  };