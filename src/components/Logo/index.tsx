import React, { ReactNode } from 'react';

import style from './logo.module.scss';

type Props = {
  size?: {
    width: number;
    height: number;
  },
  className?: string;
  type?: 'Plain' | 'Cricle';
  children?: ReactNode;  
}
function Logo(props: Props) {
  const { width = 40, height=40 } = props?.size || {};
  const { type='Plain', children, className='' } = props;

  return (
    <div className={`${type === 'Cricle' ? style.logo : style.plain} ${className} `}>
      <img src={''} width={width} height={height} />
      {children}
    </div>
  );
}

export default Logo;
