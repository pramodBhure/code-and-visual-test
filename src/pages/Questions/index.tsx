import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { fetchQuestions, submitAssigment } from '../../store/questions/actions';
import { getQuestions } from '../../store/questions/selector';

import Button from './../../components/Button';
import CheckBox from './../../components/CheckBox';

import style from './questions.module.scss';

type Props = {}
function Questions(props: Props) {
  const dispatch = useDispatch();
  const questions = useSelector(getQuestions);
  const history = useHistory();
  const [selectedQuestion, setSelectedQustion] = useState(questions);

  const handleOptionSelect = (event: any, questionId: string) => {
    const selectedOption = event.target.name && event.target.name.split("$")[1];
    
    const currentSelection = selectedQuestion.map((question:any) => {
      if (question.id === questionId) {
        return {
          ...question,
          selectedAns: selectedOption
        }
      }
      return question;
    });

    setSelectedQustion(currentSelection);
  }

  const handleSubmitResult = () => {
    dispatch(submitAssigment(selectedQuestion));
    //Should be use after then
    history.push(`/result`);
    
  };

  useEffect(() => {
    dispatch(fetchQuestions());
  }, [dispatch]);

  useEffect(() => {
    setSelectedQustion(questions);
  }, [questions]);
  
  console.log(selectedQuestion);

  return (
    <div>
      <section>
        {selectedQuestion.map((question: any) => {
          return (<>
            <h3>{question?.title}</h3>
            <div className={style.radioButtonGroup} key={question.id}>
              {question.options.map((option: any) => {
                return (
                  <div className={style.radioButton} key={`${question.id}_${option.key}`}>
                    <input type={"radio"} id={`${question.id}_${option.key}`} name={`${question.id}$${option.key}`} value={option.key} checked={(option.key === question.selectedAns)} onChange={(event) => {
                      handleOptionSelect(event, question.id)
                    }}
                  />
                    <label htmlFor={`${question.id}_${option.key}`}>{option.text}</label>
                  </div>
                  )
                })
              }
            </div>
          </>)
        })}
        
      </section>
      <Button onClick={handleSubmitResult} label={"Start Quiz"} />
    </div>
  );
}

export default Questions;
