import React from 'react';
import { useHistory } from 'react-router-dom';

import Button from './../../components/Button';
type Props = {};

function Home(props: Props) {
  const history = useHistory();

  const handleQuizStart = () => {
    history.push(`/questions`);
  };

  return (
    <div>
      <h1>Welcome to SpinWheel Quizz</h1>
      <Button onClick={handleQuizStart} label={"Start Quiz"} />
    </div>
  );
}

export default Home;
