import React, { useState, useEffect } from 'react';
import {  useSelector } from 'react-redux';
import { getResult } from '../../store/questions/selector';

type Props = {};

function Result(props: Props) {
  const result = useSelector(getResult);

  return (
    <>
      <h1>{`Yout Result is:  ${result?.finalResult}`}</h1>
      <h2>{`incorrect: ${result?.inCorrectAns}`}</h2>
      <h2></h2>
    </>
  );
}

export default Result;
