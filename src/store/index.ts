import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './rootReducer';

/* MiddleWare: logger */
const logger = (store: any) => {
    return (next:any) => {
      return (action: any) => {
        console.log('[middleWare]: dispatching', action);
        const result = next(action);
        console.log('[middleWare]: nextState', store.getState());
        return result;
      }
    }
  }

const store = createStore(rootReducer, applyMiddleware(thunk));

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type
export type AppDispatch = typeof store.dispatch

export default store;