import { createAsyncAction } from 'typesafe-actions';
import { ThunkDispatch } from 'redux-thunk';
import { fetchQuestion, postAssignment } from './api';


type Questions = any;
type Result= any;
type Error= any;

export const fetchQuestionsAsync = createAsyncAction(
    'FETCH_QUESTIONS_REQUEST',
    'FETCH_QUESTIONS_SUCCESS',
    'FETCH_QUESTIONS_FAILURE',
  )<undefined, Questions, Error>();

  export const submitAssignmentAsync = createAsyncAction(
    'SUBMIT_ASSIGNMENT_REQUEST',
    'SUBMIT_ASSIGNMENT_SUCCESS',
    'SUBMIT_ASSIGNMENT_FAILURE',
  )<undefined, Result, Error>();


  export const updateResultAsync = createAsyncAction(
    'UPDATE_RESULT_REQUEST',
    'UPDATE_RESULT_SUCCESS',
    'UPDATE_RESULT_FAILURE',
  )<undefined, Questions, Error>();


  export const fetchQuestions = () => async (dispatch: ThunkDispatch<any, any, any>) => {
    dispatch(fetchQuestionsAsync.request());
   const question =  await fetchQuestion();
    dispatch(fetchQuestionsAsync.success(question));
  }
  

  export const submitAssigment = (data:any) => async (dispatch: ThunkDispatch<any, any, any>): Promise<any> => {
    dispatch(submitAssignmentAsync.request());
    const result =  await postAssignment(data);
    console.log(result);
    dispatch(submitAssignmentAsync.success(result));
    return result;
  }
  