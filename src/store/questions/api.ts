export const fetchQuestion = async () => {
    //const response = await fetch('https://onlinetest721.herokuapp.com');
    const response = await fetch('http://localhost:3000/questions');
    const data = await response.json();
    return data;
};

export const postAssignment = async (data: any) => {
    const response = await fetch('https://onlinetest721.herokuapp.com/submit', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
          }
    });

    return response.json();
}
