import { createSelector } from 'reselect';

import { RootState } from './../index';

type Questions = any;

export const isFetching = (state: RootState)  => state.questions.isFetching;
export const getQuestion = (state: RootState)  => state.questions.items;
export const getQuestionReducer = (state: RootState)  => state.questions;

export const getQuestions = createSelector(
    getQuestion,
    (state: Questions) => {
        return state?.data || []
    })


    export const getResult = createSelector(
        getQuestionReducer,
        (state: Questions) => {
            return state?.result || 0
        })
    

