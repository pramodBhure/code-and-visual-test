import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { fetchQuestionsAsync, submitAssignmentAsync } from './actions';


export const isFetching = createReducer(false);
const init:Array<any> = [];

export const items = createReducer(init)
.handleAction(fetchQuestionsAsync.success, (state: any, action: any) => {
  return action.payload;
});

export const result = createReducer({}).handleAction(submitAssignmentAsync.success,  (state: any, action: any) => {
  console.log(action.payload)
  return action.payload
})

const questions = combineReducers({
    isFetching,
    items,
    result
  });

  export default questions;