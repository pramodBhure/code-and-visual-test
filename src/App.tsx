/***
 * Types
 */
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Switch, Route } from "react-router-dom";

import { getQuestions } from './store/questions/selector';
import style from './app.styles.module.scss';

import Questions from './pages/Questions/index';
import Result from './pages/Result/index';
import Home from './pages/Home/index';


export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/questions" component={Questions} />
      <Route exact path="/result" component={Result} />
    </Switch>
  );
}
