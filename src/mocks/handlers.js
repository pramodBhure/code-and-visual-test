import { rest } from 'msw';
import  { getAssignmentQueRequestHandler, submitAssignmentRequestHandler} from './questions' 


export const handlers = [
    rest.get('/questions', getAssignmentQueRequestHandler),
    rest.post('/submit', submitAssignmentRequestHandler),
];
