import questions from './questions.json';
import ans from './ans.json';

export const submitAssignmentRequestHandler = (req, res, ctx) => {
    const weitage=10;
    const submitedQuestions = JSON.parse(req.body);

    const correctAns = submitedQuestions.map((question) => {
       const currentQuestion = ans.ans.find((a) => a.id === question.id);

       if (currentQuestion) {
        question.options.find(a => a.key === currentQuestion.key);
       }
        
    });

    return res(ctx.json({ score: correctAns.length * weitage }));
};

export const getAssignmentQueRequestHandler = (req, res, ctx) => {
    return res(ctx.json(questions))
};